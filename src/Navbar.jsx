import { useState } from 'react';
import logo from './assets/logo.jpeg';

function Navbar() {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav className="bg-white shadow">
      <div className="container mx-auto px-4 md:px-8 lg:px-8 flex justify-between items-center py-4">
        {/* Left side */}
        <div className="flex items-center space-x-4">
          {/* Hamburger menu */}
          <button
            onClick={toggleMenu}
            className="lg:hidden text-gray-800 hover:text-black focus:outline-none"
            aria-label="Toggle mobile menu"
          >
            <svg
              className="w-6 h-6"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d={isOpen ? 'M6 18L18 6M6 6l12 12' : 'M4 6h16M4 12h16M4 18h7'}
              ></path>
            </svg>
          </button>

          {/* Logo */}
          <a href="/" className="flex-shrink-0">
            <img src={logo} alt="Logo" className="h-8 w-auto" />
          </a>

          {/* Main Navigation */}
          <div className="hidden lg:flex items-center space-x-4">
            {/* Main navigation links */}
            <div className="relative group">
              <a
                href="#"
                className="text-gray-800 hover:text-black flex items-center"
              >
                Find Designers
                <svg
                  className="ml-1 w-4 h-4"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                >
                  <path
                    fillRule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </a>
              <div className="absolute hidden group-hover:block bg-white shadow-md rounded mt-2 w-40">
                <a
                  href="#"
                  className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
                >
                  Designer 1
                </a>
                <a
                  href="#"
                  className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
                >
                  Designer 2
                </a>
                {/* Add more designer links here */}
              </div>
            </div>
            <a href="#" className="text-gray-800 hover:text-black">
              Inspiration
            </a>
            <div className="relative group">
              <a
                href="#"
                className="text-gray-800 hover:text-black flex items-center"
              >
                Courses
                <svg
                  className="ml-1 w-4 h-4"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                >
                  <path
                    fillRule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </a>
              <div className="absolute hidden group-hover:block bg-white shadow-md rounded mt-2 w-40">
                <a
                  href="#"
                  className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
                >
                  Course 1
                </a>
                <a
                  href="#"
                  className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
                >
                  Course 2
                </a>
                {/* Add more course links here */}
              </div>
            </div>
            <a href="#" className="text-gray-800 hover:text-black">
              Jobs
            </a>
            <a href="#" className="text-gray-800 hover:text-black">
              Go Pro
            </a>
          </div>
        </div>

        {/* Right side */}
        <div className="flex items-center space-x-4">
          {/* Search Input */}
          <div className="relative hidden md:block">
            <input
              type="text"
              placeholder="Search..."
              className="border border-gray-300 rounded-full py-2 px-4 pl-10 focus:outline-none focus:ring focus:border-blue-300"
            />
            <div className="absolute left-3 top-2.5 text-gray-400">
              <svg
                className="h-5 w-5"
                fill="currentColor"
                viewBox="0 0 20 20"
              >
                <path
                  fillRule="evenodd"
                  d="M12.9 14.32a8 8 0 111.41-1.41l4.33 4.32a1 1 0 01-1.41 1.42l-4.32-4.33zM8 14A6 6 0 108 2a6 6 0 000 12z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          </div>

          {/* Sign Up Button */}
          <a
            href="#"
            className="bg-black text-white rounded-full py-2 px-4 hidden md:block"
          >
            Sign Up
          </a>
        </div>
      </div>

      {/* Mobile Menu */}
      {isOpen && (
        <div className="lg:hidden">
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Find Designers
          </a>
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Inspiration
          </a>
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Courses
          </a>
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Jobs
          </a>
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Go Pro
            </a>
          <div className="border-b border-gray-300 my-1"></div>
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Log In
          </a>
          <a
            href="#"
            className="block px-4 py-2 text-gray-800 hover:bg-gray-200"
          >
            Sign Up
          </a>
        </div>
      )}
    </nav>
  );
}

export default Navbar;
